package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL(1L, "Disponivel"),
    CHECKIN(2L, "Chekin"),
    COMPRADO(3L, "Comprado");

    private long id;
    private String nome;

    SituacaoInscricaoEnum(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public long getId() {
        return this.id;
    }

}